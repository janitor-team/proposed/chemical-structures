a CGI script to implement the web service for molecules

Index: chemical-structures/convert.cgi
===================================================================
--- /dev/null
+++ chemical-structures/convert.cgi
@@ -0,0 +1,95 @@
+#!/usr/bin/python3
+
+import cgi, os, tempfile
+import cgitb; cgitb.enable()
+
+form = cgi.FieldStorage()
+
+"""
+contentTypes were made from the package chemical-mime-data with some additions
+"""
+
+contentTypes={
+	'alc':'chemical/x-alchemy',
+	'arc':'chemical/x-msi-car',
+	'c3d':'chemical/x-chem3d',
+	'cac':'chemical/x-cache',
+	'cache':'chemical/x-cache',
+	'car':'chemical/x-msi-car',
+	'cdx':'chemical/x-cdx',
+	'cif':'chemical/x-cif',
+	'cml':'chemical/x-cml',
+	'cor':'chemical/x-msi-car',
+	'dat':'chemical/x-mopac-input',
+	'dx':'chemical/x-jcamp-dx',
+	'gal':'chemical/x-gaussian-log',
+	'gam':'chemical/x-gamess-input',
+	'gamin':'chemical/x-gamess-input',
+	'gau':'chemical/x-gaussian-input',
+	'gjc':'chemical/x-gaussian-input',
+	'gjf':'chemical/x-gaussian-input',
+	'gpr':'chemical/x-gpr',
+	'gpt':'chemical/x-mopac-graph',
+	'hessian':'chemical/x-msi-hessian',
+	'hin':'chemical/x-hin',
+	'inchi':'chemical/x-inchi',
+	'inchix':'chemical/x-inchi-xml',
+	'inp':'chemical/x-gamess-input',
+	'jdx':'chemical/x-jcamp-dx',
+	'mcif':'chemical/x-mmcif',
+	'mdf':'chemical/x-msi-mdf',
+	'mdl':'chemical/x-mdl-molfile',
+	'mmd':'chemical/x-macromodel-input',
+	'mmod':'chemical/x-macromodel-input',
+	'mol':'chemical/x-mdl-molfile',
+	'mol2':'chemical/x-mol2',
+	'moo':'chemical/x-mopac-out',
+	'mop':'chemical/x-mopac-input',
+	'mopcrt':'chemical/x-mopac-input',
+	'mpc':'chemical/x-mopac-input',
+	'msi':'chemical/x-msi-msi',
+	'out':'chemical/x-mopac-out',
+	'outmol':'chemical/x-dmol',
+	'pdb':'chemical/x-pdb',
+	'rd':'chemical/x-mdl-rdfile',
+	'res':'chemical/x-shelx',
+	'rxn':'chemical/x-mdl-rxnfile',
+	'sd':'chemical/x-mdl-sdfile',
+	'sdf':'chemical/x-mdl-sdfile',
+	'tgf':'chemical/x-mdl-tgf',
+	'vmd':'chemical/x-vmd',
+	'xyz':'chemical/x-xyz',
+	'zmt':'chemical/x-mopac-input'}
+
+
+def output(mol, ext, debug=False):
+
+	if ext in contentTypes:
+		ct=contentTypes[ext]
+	else:
+		ct='chemical/unknown'
+	if debug:
+		print ("Content-type: text/html\n\n")
+	else:
+		print ("Content-type: %s\nContent-Disposition: inline; filename=\"%s\"\n\n" %(ct,mol+'.'+ext))
+	handle, outfile = tempfile.mkstemp('.'+ext)
+	if os.path.exists(mol+".cml.gz"):
+		handle1, outfile1 = tempfile.mkstemp('.cml')
+		os.system("zcat %s.cml.gz > %s" %(mol, outfile1))
+		os.system("babel -icml %s -o%s %s" %(outfile1, ext, outfile))
+		os.remove(outfile1)
+	else:
+		os.system("babel -icml %s.cml -o%s %s" %(mol, ext, outfile))
+	print (''.join(open(outfile).readlines()))
+	os.remove(outfile)
+
+
+
+if __name__ == "__main__":
+	if "mol" not in form or "ext" not in form:
+		print("<H1>Error</H1>")
+		print("Please fill in the molecules name and extension fields.")
+		output("src/water/water", "mol")
+	else:
+		output(form["mol"].value, form["ext"].value)
+
