deactivate the jmol applet feature
Index: chemical-structures-2.2.dfsg.0/src/jmol/Jmol.js
===================================================================
--- chemical-structures-2.2.dfsg.0.orig/src/jmol/Jmol.js
+++ chemical-structures-2.2.dfsg.0/src/jmol/Jmol.js
@@ -158,7 +158,12 @@ function jmolSetAppletWindow(w) {
 
 function jmolApplet(size, script, nameSuffix) {
   _jmolInitCheck();
-  return _jmolApplet(size, null, script, nameSuffix);
+  /// the applets have been removed from the main repository of Debian
+  /// they must be provided by another package
+  //////////////////
+  /// return _jmolApplet(size, null, script, nameSuffix);
+  d=window.document.getElementById("structure");
+  d.innerHTML="(no Jmol applet featured)";
 }
 
 ////////////////////////////////////////////////////////////////
